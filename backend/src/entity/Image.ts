import {Entity, PrimaryGeneratedColumn, Column} from "typeorm";

@Entity()
export class Image {

    @PrimaryGeneratedColumn()
    id: number;

    @Column()
    name:string;

    @Column()
    size:number;

    @Column()
    recognitionResult: string;

    @Column()
    downloadLink:string;

}
