import { EvaluateController } from "./controller/EvaluateController";
import { ImageController } from "./controller/ImageController";
import { UserController } from "./controller/UserController";

export const Routes = [
  {
    method: "get",
    route: "/users",
    controller: UserController,
    action: "all",
  },
  {
    method: "post",
    route: "/users/login",
    controller: UserController,
    action: "login",
  },
  {
    method: "post",
    route: "/users",
    controller: UserController,
    action: "save",
  },
  {
    method: "delete",
    route: "/users/:id",
    controller: UserController,
    action: "remove",
  },
  {
    method: "get",
    route: "/images",
    controller: ImageController,
    action: "all",
  },
  {
    method: "post",
    route: "/images",
    controller: ImageController,
    action: "save",
  },
  {
    method: "post",
    route: "/evaluate",
    controller: EvaluateController,
    action: "upload",
  },
  {
    method: "post",
    route: "/train",
    controller: EvaluateController,
    action: "train",
  },
];
