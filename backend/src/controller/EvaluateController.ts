import * as tf from "@tensorflow/tfjs-node";
import * as tfvis from "@tensorflow/tfjs-vis";
import { NextFunction, Request, Response } from "express";
import { getRepository } from "typeorm";
import { MnistData, toArrayBuffer } from "../evaluation-data/data";
import { getModel, train } from "../evaluation-data/script";
import { Image } from "../entity/Image";

const IMAGE_WIDTH = 28;
const IMAGE_HEIGHT = 28;
const IMAGE_CHANNELS = 1;
const testDataSize = 1;

export class EvaluateController {
  private imageRepository = getRepository(Image);
  classNames = [
    "Zero",
    "One",
    "Two",
    "Three",
    "Four",
    "Five",
    "Six",
    "Seven",
    "Eight",
    "Nine",
  ];

  constructor() {}

  async evaluateImg(request: Request, response: Response, next: NextFunction) {
    try {
      const model = await tf.loadLayersModel(
        "file://C:/Users/Mihai/Desktop/Anul3Sem2/Fullstack/repo-fsd/tema2/backend/src/evaluation-data/model.json"
      );
      const data = new MnistData();
      await data.load();
      const [preds, labels] = this.doPrediction(model, data, 1);
      return response.send(`${labels} --------- ${preds}`);
    } catch (exception) {
      console.log(exception);
      response.sendStatus(500);
    }
  }

  async upload(request: Request, response: Response, next: NextFunction) {
    try {
      const file = request.files.file;
      console.log("file name: ",file.name);
      const buffer = file.data;
      const data = new MnistData();
      const arrayBuffer = toArrayBuffer(buffer);

      await data.newLoad(arrayBuffer);

      const model = await tf.loadLayersModel(
        "file://D:/Facultate/ANUL III.SEM 2/FullStack ML/Lab2Repo/tema2/backend/src/evaluation-data/model.json"
      );

      const testData = data.nextTestBatch(testDataSize);

      const testxs = testData.xs.reshape([
        testDataSize,
        IMAGE_WIDTH,
        IMAGE_HEIGHT,
        1,
      ]);

      const labels = testData.labels.argMax(-1);
      const preds: any = model.predict(testxs);
      testxs.dispose();


      await preds
        .data()
        .then((data: any[]) => {

          const image:Image={
            id:undefined,
            name: file.name,
            size: file.size,
            recognitionResult: data.indexOf(Math.max(...data)).toString(),
            downloadLink: "coming soon",
          }
          console.log(image)
          this.imageRepository.save(image);
          return response.json({ gj: 'gj' });
        })
        .catch((err) => {
          return response.sendStatus(500);
        });
    } catch (err) {
      console.log(err);
      response.sendStatus(500);
    }
  }

  async train(req: Request, res: Response, next: NextFunction) {
    try {
      const data = new MnistData();
      await data.load();
      const model = getModel();
      await train(model, data).then((results) => {
        model.save(
          "file://C:/Users/Mihai/Desktop/Anul3Sem2/Fullstack/repo-fsd/tema2/backend/src/evaluation-data/trained-model.json"
        );
        res.send(results);
      });
    } catch (err) {
      console.log(err);
      res.sendStatus(500);
    }
  }

  doPrediction(model, data, testDataSize = 500) {
    const IMAGE_WIDTH = 28;
    const IMAGE_HEIGHT = 28;
    const testData = data.nextTestBatch(testDataSize);
    const testxs = testData.xs.reshape([
      testDataSize,
      IMAGE_WIDTH,
      IMAGE_HEIGHT,
      1,
    ]);
    const labels = testData.labels.argMax(-1);
    const preds = model.predict(testxs);

    testxs.dispose();
    return [preds, labels];
  }

  async showAccuracy(model, data) {
    const [preds, labels] = this.doPrediction(model, data);
    const classAccuracy = await tfvis.metrics.perClassAccuracy(labels, preds);
    const container = { name: "Accuracy", tab: "Evaluation" };
    tfvis.show.perClassAccuracy(container, classAccuracy, this.classNames);

    labels.dispose();
  }

  async showConfusion(model, data) {
    const [preds, labels] = this.doPrediction(model, data);
    const confusionMatrix = await tfvis.metrics.confusionMatrix(labels, preds);
    const container = { name: "Confusion Matrix", tab: "Evaluation" };
    tfvis.render.confusionMatrix(container, {
      values: confusionMatrix,
      tickLabels: this.classNames,
    });

    labels.dispose();
  }
}
