import { getRepository } from 'typeorm';
import { NextFunction, Request, Response } from 'express';
import { Image } from '../entity/Image';
import * as jwt from 'jsonwebtoken';

export class ImageController {
  private imageRepository = getRepository(Image);

  async all(request: Request, response: Response, next: NextFunction) {
    return this.imageRepository.find();
  }

  async save(request: Request, response: Response, next: NextFunction) {
    return this.imageRepository.save(request.body);
  }

}
