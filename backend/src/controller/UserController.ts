import { getRepository } from 'typeorm';
import { NextFunction, Request, Response } from 'express';
import { User } from '../entity/User';
import * as jwt from 'jsonwebtoken';
import {secret} from '../utils/env'
import { checkJwt } from '../utils/JwtChecker';

export class UserController {
  private userRepository = getRepository(User);

  async all(request: Request, response: Response, next: NextFunction) {
    return this.userRepository.find();
  }

  async login(request: Request, response: Response, next: NextFunction) {
    try {
      const { email, password } = request.body;
      const user = this.userRepository.findOne({
        where: { email: email, password: password },
      });
      user
        .then((user) => {
          if (user !== null) {
            const token = jwt.sign({ user }, secret, {
              expiresIn: '5h',
            });
            console.log(token);
            response.json(token);
          } else {
            response.json(null);
          }
        })
        .catch((err) => console.log('ERROR ', err));
    } catch (exception) {
      console.log('EXCEPTION ', exception);
    }
  }

  async save(request: Request, response: Response, next: NextFunction) {
    return this.userRepository.save(request.body);
  }

  async remove(request: Request, response: Response, next: NextFunction) {
    let userToRemove = await this.userRepository.findOne(request.params.id);
    await this.userRepository.remove(userToRemove);
  }
}
