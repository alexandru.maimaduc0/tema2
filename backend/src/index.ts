import "reflect-metadata";
import {createConnection} from "typeorm";
import * as express from "express";
import * as bodyParser from "body-parser";
import {Request, Response} from "express";
import {Routes} from "./routes";
import {User} from "./entity/User";
import * as jwt from "express-jwt";
import * as cors from 'cors';
import {secret, cryptingAlgorithm} from './utils/env'
import * as fileUpload from 'express-fileupload';

createConnection().then(async connection => {

    const corsOptions={
      "origin": "*",
      "methods": "GET,HEAD,PUT,PATCH,POST,DELETE",
      "preflightContinue": false,
      "optionsSuccessStatus": 204
    }
    // create express app
    const app = express();

    app.use(jwt({ secret: secret, algorithms: cryptingAlgorithm}).unless({path: ['/users/login','/evaluate']}));
    app.use(express.json());
    app.use(express.urlencoded({extended:true}));

    app.use(fileUpload())


    app.use(cors(corsOptions));
    app.use(function (err, req, res, next) {
        if (err.name === "UnauthorizedError") {
          console.log(err);
          res.sendStatus(401);
        }
      });

    // register express routes from defined application routes
    Routes.forEach(route => {
        (app as any)[route.method](route.route, (req: Request, res: Response, next: Function) => {
            const result = (new (route.controller as any))[route.action](req, res, next);
            if (result instanceof Promise) {
                result.then(result => result !== null && result !== undefined ? res.send(result) : undefined);

            } else if (result !== null && result !== undefined) {
                res.json(result);
            }
        });
    });


    // start express server
    app.listen(3000);


    console.log("Express server has started on port 3000. Open http://localhost:3000/users to see results");

}).catch(error => console.log(error));
