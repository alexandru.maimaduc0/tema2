import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { HeaderStateService } from '../services/header-state/header-state.service';
import { Router } from '@angular/router';
import { RestRequestService } from '../services/rest-request/rest-request.service';
import { UserService } from '../services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  profileForm= new FormGroup({
    email: new FormControl('',[Validators.required,Validators.minLength(4)]),
    password: new FormControl('',[Validators.required,Validators.minLength(6)]),
  })



  constructor(
    private headerStateService:HeaderStateService,
    private router:Router,
    private userService:UserService) {

   }

  ngOnInit(): void {
  }

  onLoginClick(): void{
    this.profileForm.markAsTouched();
    if(this.profileForm.valid){
      this.userService.login(this.profileForm.getRawValue()).subscribe(result => {
        if(result!==null){
          this.headerStateService.show();
          this.router.navigateByUrl("/table")
        }else{
          this.headerStateService.hide();
        }
      });
      this.router.navigateByUrl("/table")
    }
  }

}
