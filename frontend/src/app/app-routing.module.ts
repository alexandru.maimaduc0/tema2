import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from './app.component';
import { LoginGuardGuard } from './guards/login-guard.guard';
import { HomeComponent } from './home/home.component';
import { LoginComponent } from './login/login.component';
import { TableComponent } from './table/table.component';
import { UploadComponent } from './upload/upload.component';
import { EvaluateComponent } from './evaluate/evaluate.component'

const routes: Routes = [
  { path: "table", canActivate:[LoginGuardGuard], component: TableComponent },
  { path: "upload", canActivate:[LoginGuardGuard], component: UploadComponent },
  { path: "evaluate", canActivate:[LoginGuardGuard], component: EvaluateComponent },
  { path: "", component: HomeComponent },
  { path: "**", redirectTo: "" },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
