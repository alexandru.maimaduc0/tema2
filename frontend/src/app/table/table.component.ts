import { Component, OnInit } from '@angular/core';
import { Image } from '../models/image';
import { ImageService } from '../services/image/image.service';

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.scss']
})

export class TableComponent implements OnInit {

  ELEMENT_DATA: Image[] = []
  displayedColumns: string[] = ['imageName', 'size', 'recognitionResult', 'downloadLink'];
  constructor(private imageService:ImageService) { }

  ngOnInit(): void {
    this.imageService.getAll().subscribe(result => {
      this.ELEMENT_DATA = result
    });
  }
}
