import { Component, Input, OnInit } from '@angular/core';
import { Image } from '../models/image';
import { ImageService } from '../services/image/image.service';
@Component({
  selector: 'app-upload',
  templateUrl: './upload.component.html',
  styleUrls: ['./upload.component.scss'],
})
export class UploadComponent implements OnInit {
  imageSrc: string = '';
  reader: FileReader;

  constructor(private imageService: ImageService) {
    this.reader = new FileReader();
  }

  ngOnInit(): void {
    this.imageSrc = 'assets/images/corgi.jpg';
    console.log(localStorage.getItem('jwt-token'));
  }
  handleFileInput(event: any) {
    const currentPhoto = event.target.files.item(0);
    this.imageService.addImage(currentPhoto).subscribe(res => {
      console.log(res);
    })
  }

    // this.reader.readAsDataURL(event.target.files[0]);
    // this.reader.onload= (event:any) =>{
    //     this.imageSrc=event.target.result
    // }
}
