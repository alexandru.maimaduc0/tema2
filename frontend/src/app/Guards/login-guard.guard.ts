import { Injectable } from '@angular/core';
import { CanActivate, UrlTree, Router } from '@angular/router';
import { Observable, of } from 'rxjs';
import { UserService } from '../services/user/user.service';
import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root',
})
export class LoginGuardGuard implements CanActivate {
  constructor(
    private userService: UserService,
    private jwtHelperService: JwtHelperService,
    private router: Router
  ) {}

  canActivate(): Observable<boolean | UrlTree> {
    const token = this.userService.getToken();
    if (token !== null && !this.jwtHelperService.isTokenExpired(token)) {
      return of(true);
    }
    this.router.navigateByUrl('');
    return of(false);
  }
}
