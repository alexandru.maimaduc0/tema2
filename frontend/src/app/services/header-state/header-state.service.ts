import { EventEmitter, Injectable, Input, Output } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class HeaderStateService {

  header = new EventEmitter<boolean>();

  constructor() {
  }

  show() {
    this.header.emit(true)
  }

  hide() {
    this.header.emit(false)
  }

}
