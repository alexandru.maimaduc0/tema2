import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';

@Injectable({
  providedIn: 'root',
})
@Injectable()
export class RestRequestService {
  token = '';
  readonly baseUrl = 'http://localhost:3000';
  readonly httpOptions = {
    headers: new HttpHeaders({
      'Content-type': 'application/json',
      Authorization: 'Bearer ' + localStorage.getItem('jwt-token')!!,
    }),
  };

  constructor(private httpClient: HttpClient) {}

  get(url: string): Observable<string> {
    return this.httpClient.get<string>(this.baseUrl + url, this.httpOptions);
  }

  post(obj: any, url: string): Observable<any> {
    const formData: FormData = new FormData();
    formData.append('file', obj);
    return this.httpClient.post(
      this.baseUrl + url,
      formData,
    this.httpOptions
    );
  }

  postImage(file: File){
    const formData: FormData = new FormData();
    formData.append('file',file);
    return this.httpClient.post(`${this.baseUrl}/evaluate`,formData);
  }
}
