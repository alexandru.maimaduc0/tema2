import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { Image } from 'src/app/models/image';
import { User } from 'src/app/models/user';
import { RestRequestService } from '../rest-request/rest-request.service';

@Injectable({
  providedIn: 'root'
})
export class ImageService {

  image: Image = {
    name: '',
    size: 0,
    recognitionResult: '',
    downloadLink: ''
  };

  readonly baseUrl = "";
  constructor(
    private httpClient: HttpClient,
    private restService: RestRequestService
  ) { }

  addImage(image: File) {
    return this.restService.postImage(image);
  }

  getAll(){
    return this.restService.get('/images').pipe(
      tap((result:any)=>{
      })
    )
  }

  getToken() {
    return localStorage.getItem("jwt-token");
  }
}
