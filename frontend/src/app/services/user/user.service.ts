import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import {tap} from "rxjs/operators";
import { User } from 'src/app/models/user';
import { RestRequestService } from '../rest-request/rest-request.service';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  user:User={
    email:'',
    password:'',
  };

  readonly baseUrl="";
  readonly httpOptions= {
    headers:new HttpHeaders({
      'Content-type':'application/json',
    })
  };
  constructor(
    private httpClient:HttpClient,
    private restService:RestRequestService
  ) { }

  login(user: User) {
    return this.restService.post(user,'/users/login').pipe(
      tap((result: string) => {
        this.setToken(result);
      })
    );
  }

  setToken(token: string) {
    localStorage.setItem("jwt-token", token);
  }

  getToken() {
    return localStorage.getItem("jwt-token");
  }
}

