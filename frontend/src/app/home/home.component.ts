import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { HeaderStateService } from '../services/header-state/header-state.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit, OnDestroy {
  subscription:Subscription;
  show:boolean=false;

  constructor(
    private headerStateService: HeaderStateService
  ) {
    this.subscription=headerStateService.header.subscribe(data=>{
      this.show=data;
    })
  }
  
  ngOnDestroy(): void {
    if(this.subscription){
      this.subscription.unsubscribe()
    }
  }

  ngOnInit(): void {
  }
  

}
