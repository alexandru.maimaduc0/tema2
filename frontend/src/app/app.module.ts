import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './login/login.component';
import { TableComponent } from './table/table.component';
import { UploadComponent } from './upload/upload.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HeaderComponent } from './header/header.component';
import { HomeComponent } from './home/home.component';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatIconModule } from '@angular/material/icon';
import { MatCardModule } from '@angular/material/card';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import {MatTableModule} from '@angular/material/table';
import { ReactiveFormsModule } from '@angular/forms';
import { HeaderStateService } from './services/header-state/header-state.service';
import { RestRequestService } from './services/rest-request/rest-request.service';
import { HttpClientModule } from '@angular/common/http';
import {JwtModule} from '@auth0/angular-jwt';
import { EvaluateComponent } from './evaluate/evaluate.component';
import { DatastoreComponent } from './datastore/datastore.component';


export function tokenGetter(){
  return localStorage.getItem("jwt-token");
}


@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    TableComponent,
    UploadComponent,
    HeaderComponent,
    HomeComponent,
    EvaluateComponent,
    DatastoreComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MatFormFieldModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatButtonModule,
    MatToolbarModule,
    MatTableModule,
    ReactiveFormsModule,
    HttpClientModule,
    JwtModule.forRoot({
      config:{
      tokenGetter:tokenGetter
    }})
  ],
  providers: [HeaderStateService,RestRequestService],
  bootstrap: [AppComponent]
})
export class AppModule { }
