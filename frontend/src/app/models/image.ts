export class Image{
    public name='';
    public size=0;
    public recognitionResult='';
    public downloadLink='';

    constructor(name:string,size:number,recognitionResult:string,downloadLink:string) {
        this.name=name;
        this.size=size;
        this.recognitionResult=recognitionResult;
        this.downloadLink=downloadLink;
    }
}
