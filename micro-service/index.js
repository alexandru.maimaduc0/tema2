// exports.helloWorld = (req, res) => {
//     res.send('Hello, World');
// };


exports.helloWorld = async (req, res) => {
    const {Datastore} = require('@google-cloud/datastore');
    const datastore = new Datastore({
        projectId: 'fsdproj-310918',
        keyFilename: 'google.json'
    });
    const taskKey = datastore.key('user');
    const [entity] = await datastore.get(taskKey);
    res.send(entity);
};